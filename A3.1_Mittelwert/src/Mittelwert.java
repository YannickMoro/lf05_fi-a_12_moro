import java.util.Scanner;

public class Mittelwert {
   public static double liesDouble(String eingabetext) {
	   Scanner myScanner = new Scanner(System.in);
	   System.out.println(eingabetext);
	   double wert = myScanner.nextDouble();
	   return wert;
   }
   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = liesDouble("Hier ersten Wert eingeben:  ");
      double y = liesDouble("Hier zweiten Wert eingeben:  ");
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = (x + y) / 2.0;
      m = berechnenMittelwert(x,y);
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      	mittelwertAusgeben(x,y,m);
      // =================================
      
   }
   public static double berechnenMittelwert(double wert1, double wert2) {
	   double erg = (wert1 +wert2)/2;
	   return erg;
   }
   
   public static void mittelwertAusgeben(double wert1, double wert2, double mittelwert) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", wert1, wert2, mittelwert);
	   
   }
   
}
