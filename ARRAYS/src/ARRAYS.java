import java.util.Arrays;

public class ARRAYS {

	public static void main(String[] args) {
		
		System.out.println("Hier lassen sich alle Werte in dem Array anzeigen\n");
		holeArrayWerte();
		
		System.out.println("Hier wird jeder einzelne Wert ausgedruckt im Array");
		verbesserteAusgabe();
	}
	
	
	public static void holeArrayWerte(){
		
		
		double[] temperatur = new double [5];
		
		temperatur[0] = 4.0;
		temperatur[1] = 5.0;
		temperatur[2] = 6.0;
		temperatur[3] = 7.0;
		temperatur[4] = 8.0;
		
		System.out.println(Arrays.toString(temperatur));
		
	}
	
	public static void verbesserteAusgabe() {
		
		double[] temperatur = new double [5];
		
		temperatur[0] = 4.0;
		temperatur[1] = 5.0;
		temperatur[2] = 6.0;
		temperatur[3] = 7.0;
		temperatur[4] = 8.0;
		
		for(int zeile = 0; zeile < temperatur.length; zeile ++) {
			
			System.out.print(temperatur[zeile]+ " ");
			
		}
		
		
	
	}
	
	
}
