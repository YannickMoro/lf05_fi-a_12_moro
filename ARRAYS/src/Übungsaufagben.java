import java.util.Arrays;
import java.util.Scanner;

public class �bungsaufagben {

	public static void main(String[] args) {
		System.out.println("_______1_______\n");
		aufgabe_1();
		System.out.println("_______2_______\n");
		aufgabe_2();
		System.out.println("_______3_______\n");
		//aufgabe_3();
		System.out.println("_______4_______\n");
		aufgabe_4();
	}

	public static void aufgabe_1() {

		// Schreiben Sie ein Programm �Zahlen�, in welchem ein ganzzahliges Array der
		// L�nge 10
		// deklariert wird. Anschlie�end wird das Array mittels Schleife mit den Zahlen
		// von 0 bis 9
		// gef�llt. Zum Schluss geben Sie die Elemente des Arrays wiederum mit einer
		// Schleife auf der
		// Konsole aus.

		int x = 0;

		int Zahlen[] = new int[10];

		for (int index = 0; index < Zahlen.length; index++) {

			Zahlen[index] = x++;

		}
		// Das gef�llte Array wird abgerufen
		for (int j = 0; j < Zahlen.length; j++) {

			System.out.println(Zahlen[j]);
		}

	}

	// Das zu schreibende Programm �UngeradeZahlen� ist �hnlich der Aufgabe 1. Sie
	// deklarieren
	// wiederum ein Array mit 10 Ganzzahlen. Danach f�llen Sie es mit den ungeraden
	// Zahlen von 1 bis 19
	// und geben den Inhalt des Arrays �ber die Konsole aus (Verwenden Sie
	// Schleifen!).
	public static void aufgabe_2() {

		int Array[] = new int[10];

		for (int index = 0; index < Array.length; index++) {

			for (int j = 1; j < 20; j++) {

				if (j % 2 != 0) {

					Array[index] = j;

				}

				else {
					index++;
				}
			}

		}
		System.out.println(Arrays.toString(Array));
	}

	// Methode zum eingeben von Zeichen
	public static String scanner_Zeichen() {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Hier 5 Zeichen eingebe ");
		String Zeichen = scanner.nextLine();

		return Zeichen;

	}

	public static void aufgabe_3() {

		String Array[] = new String[5];

		for (int index = 0; index < Array.length; index++) {

			Array[index] = scanner_Zeichen();

		}

		System.out.println(Arrays.toString(Array));

		for (int j = 4; j >= 0; j--) {

			System.out.println(Array[j]);
		}
	}
	public static void aufgabe_4() {
		
		int lottozahlen [] = {3, 7, 12, 18, 37, 42};
		System.out.print("[ ");
		for (int index = 0; index < lottozahlen.length ; index++) {
			
			
			System.out.print(lottozahlen[index]+" ");
			
		}
		
		System.out.print("]");
		
	}
}
