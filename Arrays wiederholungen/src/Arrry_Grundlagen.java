import java.util.Arrays;

public class Arrry_Grundlagen {

	public static void main(String[] args) {
		// Grundlagen zu den Arrays

		// Ein Array wird definiert mit dem Befehl []Name = new "datentyp"[L�nge]
		// double [] temperatur = new double[24];

		// temperatur[0] = 4.0;
		// temperatur[1] = 4.0;
		// temperatur[2] = 4.0;
		// temperatur[3] = 4.0;

		// Nicht alle Arrays lassen sich anzeigen. Deshalb kann mit dem Befehl
		// "Array.toString(Name)" das Anzeigen vereinfacht werden
		// System.out.println(Arrays.toString(temperatur));

		holeLottoZahlen();
	}

	// Aufgabe 1
	public static void holeZahlenArray() {

		int[] zahlen = new int[10];

		for (int i = 0; i < 10; ++i) {

			zahlen[i] = i;
			System.out.println(i);
		}

		System.out.println(Arrays.toString(zahlen));

	}

	// Aufgabe 4
	public static void holeLottoZahlen() {

		int[] Lottozahlen = new int[6];

		Lottozahlen[0] = 3;
		Lottozahlen[1] = 7;
		Lottozahlen[2] = 12;
		Lottozahlen[3] = 18;
		Lottozahlen[4] = 37;
		Lottozahlen[5] = 42;

		System.out.println(Arrays.toString(Lottozahlen));

		boolean x = false;

		for (int i = 0; i < Lottozahlen.length; i++) {

			if (Lottozahlen[i] == 12) {
				System.out.println("Die Zahl 12 ist in der Ziehung enthalten");
			}
			if (Lottozahlen[i] == 13) {
				x = true;

			}

		}

		if (x) {
			System.out.println("Die Zahl 13 ist enthalten");
		}

		else {
			System.out.println("Die Zahl 13 ist nicht enthalten");
		}

	}

}
