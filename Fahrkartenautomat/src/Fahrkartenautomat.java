﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rückgabebetrag;
		int anzahlderTickets;
		double Ticketpreis;
		String x = "Euro";
		boolean endlos = true; 
		
		
		/////////////////////////////////////////////////////////////////////
		while(endlos = true) {
		double ticketauswahl = erfasseTicketpreis();
		zuZahlenderBetrag = fahrkartenbestellungErfassen(ticketauswahl);
		eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rückgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);}
		/////////////////////////////////////////////////////////////////////
		
		
	}
	
	public static int validInt()
	{
		Scanner tastatur = new Scanner(System.in);
		boolean valid= false;
		int p = 0;
		while(!valid) {
			if(tastatur.hasNextInt()) {
				p = tastatur.nextInt();
				valid = true;
			} else {
				System.out.println("Buchstaben sind nicht gültig !");
				tastatur.next();
			}
		}
		return p;
		
	}
	public static double erfasseTicketpreis() {
		
		Scanner tastatur = new Scanner(System.in);
		double ticketPreis = 0;
		int ticketwahl = 0;
		while(ticketwahl > 3 || ticketwahl < 1) {
		
			
			System.out.print("Fahrkartenbestellvorgang:\n"
					+ "=========================\n"
					+ "\n"
					+ "Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
					+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
					+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
					+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n"
					+ "");
			
			 ticketwahl = validInt();	
			
			
			
		if(ticketwahl == 1) {
			 ticketPreis = 2.90;
			
		} else if (ticketwahl == 2) {
			ticketPreis = 8.60;
			
		} else if (ticketwahl == 3) {
			ticketPreis = 23.50;
			
		} else {
			System.out.println("Diese Eingabe ist ungültig");
			
		}
		}	
		return ticketPreis;
		
	}	
	
	
	/////////////////////////////////////////////////////////////////////////
	//Geldbetrag, Tickets, Abfrage der Ticketanzahl 
	public static double fahrkartenbestellungErfassen(double ticketauswahl) {
		Scanner tastatur = new Scanner(System.in);
		
		 
		
	
		System.out.print("Die Anzahl der Tickers: ");
		int anzahlderTickets = validInt();
		
		if(anzahlderTickets > 10 || anzahlderTickets < 0) {
			anzahlderTickets = 1;	
			double zuZahlenderBetrag = ticketauswahl * anzahlderTickets;
			System.out.println("\n\nEs dürfen nicht mehr als 10 Tickets gekaut werden \nOder geben sie einen Wert größer als [0] an\nIhre Eingabe wird mit [1] Ticket ersetzt !!");
			return zuZahlenderBetrag;	
		}	
		
		
		else {
		
		double zuZahlenderBetrag = ticketauswahl * anzahlderTickets;

		return zuZahlenderBetrag;
		}
	
}
	/////////////////////////////////////////////////////////////////////////
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		
		Scanner tastatur = new Scanner(System.in);
		String x = "Euro";
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			
			System.out.printf("Noch zu zahlen: %.2f %s\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag), x);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
			double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
			
			
		}
		return eingezahlterGesamtbetrag;
		

	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=->");
			try {
				Thread.sleep(250);
			} 
			catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
			System.out.println("\n\n");
	}
	
      
	public static double rückgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag >= 0.0)

		{
//			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rückgabebetrag );
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag > 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}
		return rückgabebetrag;

	}

}
