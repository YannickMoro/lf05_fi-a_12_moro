import java.util.Scanner;

public class Aufgabe2 {
//	Ein Hardware-Gro�h�ndler m�chte den Rechnungsbetrag einer Bestellung f�r PC-M�use mit
//	einem Programm ermitteln. Wenn der Kunde mindestens 10 M�use bestellt, erfolgt die
//	Lieferung frei Haus, bei einer geringeren Bestellmenge wird eine feste Lieferpauschale von
//	10 Euro erhoben. Vom Gro�h�ndler werden die Anzahl der bestellten M�use und der
//	Einzelpreis eingegeben. Das Programm soll den Rechnungsbetrag (incl. MwSt.) ausgeben.
	
	
	
	public static void main(String[] args) {
		//////////////////////////////////////////////////////////////////////////////////
		int bestellMenge = maeuseHolen();
		int st�ckpreis = preisHolen();
		//////////////////////////////////////////////////////////////////////////////////
		double lieferkosten = 10;
		double preis = (bestellMenge * st�ckpreis)*1.19;
		
		if(bestellMenge >= 10 ) {
			System.out.println("***Lieferung erfolgt frei Haus***");
			System.out.println("*** "+ preis + "Euro incl MWST***");
		}
		
		else if(bestellMenge < 10 ) {
			double gesamtPreis = preis + lieferkosten;
			
			System.out.println("***Lieferkosten betragen 10 Euro***");
			System.out.println("*** Der Gesamte Betrag mit Lieferkosten betr�gt "+ gesamtPreis + " Euro incl MWST ***");
		}
		
		
		
		
		
		
	}
	public static int maeuseHolen() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Wieviele M�use willst du kaufen ");
		int st�ckzahl = scanner.nextInt();
		
		return st�ckzahl;
	}
	public static int preisHolen() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Der Preis f�r die M�use ");
		int preis = scanner.nextInt();
		
		return preis;
	}
	
	
	
}

