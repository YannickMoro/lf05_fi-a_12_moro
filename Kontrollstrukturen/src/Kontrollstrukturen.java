import java.util.Scanner;

public class Kontrollstrukturen {

	public static void main(String[] args) {
	////////////////////////////////////////////////
		int geldAusgabe = eingabeGeld();
		String steuerAbfrage = steuerAbfrage();
	////////////////////////////////////////////////
	
		double mehrwertSteuer = 1.19;
		
		double erm��igteSteuer = 1.07;
		
		if(steuerAbfrage.equals("J") ) {
			
			double ergebniss = mehrwertSteuer*geldAusgabe;
			System.out.println(ergebniss+ " Euro");
			
		}
		else if(steuerAbfrage.equals("N") ) {
			
			double ergebniss_1 = erm��igteSteuer*geldAusgabe;
			System.out.println(ergebniss_1 + " Euro");

		}
	}

	public static int eingabeGeld() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Hier einen Geldbetrag eingeben ");
		int betrag = scanner.nextInt();
		
		return betrag;
	}
	
	public static String steuerAbfrage() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Voller Steuersatz[J] | Erm��igter Steuersatz[N] ");
		String steuersatz = scanner.nextLine();
		
		return steuersatz;
	}
}
