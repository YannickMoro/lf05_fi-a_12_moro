import java.util.Scanner;

public class Übungsaufgabe_Consolenscanner2 {

	public static void main(String[] args) {
		System.out.println("\nHallo Nutzer bitte geben sie ihren Namen und ihr Alter ein");
		// Neues Scanner-Objekt myScanner wird erstellt
		 Scanner myScanner = new Scanner(System.in);

		 System.out.print("\nBitte geben Sie ihren Namen ein: ");

		 // Die Variable zahl1 speichert die erste Eingabe
		 String Name = myScanner.nextLine();

		 System.out.print("\nBitte geben Sie ihr Alter ein: ");

		 // Die Variable zahl2 speichert die zweite Eingabe
		 int zahl = myScanner.nextInt();
		 
	
		 System.out.println("\n\n\n Guten Tag " + Name + " du bist " + zahl +" Jahre alt: \n ***Willkommen auf unserer Seite***");

	}

}
