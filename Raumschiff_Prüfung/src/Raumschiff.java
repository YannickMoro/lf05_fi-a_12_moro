import java.util.ArrayList;

public class Raumschiff {
	private String Schiffname;
	private int HuelleInProzent;
	private int LebenserhaltungssystemInProzent;
	private int AndroidenAnzahl;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private Ladungen ladungen;
	private ArrayList<Ladungen> Ladung = new ArrayList<Ladungen>();
	private String abschießenPhotonentorpedos;
	private int EnergieversorgungInProzent;
	
	public Raumschiff() {
		
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu) {
		this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
	}
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	public String getSchiffname() {
		return Schiffname;
	}

	public void setSchiffname(String schiffname) {
		Schiffname = schiffname;
	}

	public int getHuelleInProzent() {
		return HuelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		HuelleInProzent = huelleInProzent;
	}

	public int getLebeneserhaltungssystemInProzent() {
		return LebenserhaltungssystemInProzent;
	}

	public void setLebeneserhaltungssystemInProzent(int lebeneserhaltungssystemInProzent) {
		LebenserhaltungssystemInProzent = lebeneserhaltungssystemInProzent;
	}

	public int getAndroidenAnzahl() {
		return AndroidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.AndroidenAnzahl = androidenAnzahl;
	}

	public void addLadung(Ladungen neueLadung) {
		Ladung.add(neueLadung);
	}
	//Gibt alle Parameter zu dem Raumschiff werden aufgerufen
	public void zustandausgeben() {
		System.out.println("_____"+this.Schiffname+"_____");
		System.out.println("Torpedos: "+this.photonentorpedoAnzahl);
		System.out.println("Energieversorgung: "+this.energieversorgungInProzent);
		System.out.println("Schilde: "+this.schildeInProzent);
		System.out.println("Lebenserhaltungsysteme: "+this.LebenserhaltungssystemInProzent);
		
		System.out.println("Androiden: "+this.AndroidenAnzahl);
	
		
	}
	public void ladungsausgabe() {
		System.out.println("Ladungen: "+ Ladung);
	}
	//Torpedos werden abgefeuert und anschließend erfolgt die Ausgabe
	public void abschießenPhotonentorpedos(Raumschiff r) {
		if(photonentorpedoAnzahl < 1) {
			nachrichtAnAlle("-=click=-");
		}
		else if(photonentorpedoAnzahl >= 1) {
			photonentorpedoAnzahl --;
			System.out.println("--Photonentorpedo abgeschossen--");
			System.out.println("Wir besitzen jetzt: "+photonentorpedoAnzahl+" Torpedos");
			treffer(r);
		}
		}
		
	
		//Torpedos werden abgefeuert und anschließend erfolgt die Ausgabe
		public void abschießenPhaserkanone(Raumschiff r) {
			if( energieversorgungInProzent < 50) {
				nachrichtAnAlle("-=click=-");
			}
			else if( energieversorgungInProzent >= 50) {
				energieversorgungInProzent --;
				System.out.println("--Phaser abgeschossen--");
				System.out.println( "Wir besitzen jetzt: "+ energieversorgungInProzent + " Phaser");
				treffer(r);
			} 
		
		}
	
	public void treffer(Raumschiff r) {
		System.out.println(r.getSchiffname()+" wurde getroffen !");
	
	}
	
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
		
	}
}
