
public class Raumschiff_test {

	public static void main(String[] args) {
		
		Raumschiff romulaner = new Raumschiff();
		Raumschiff klingonen = new Raumschiff();
		Raumschiff vulkanier = new Raumschiff();
		
		//Romulaner
		romulaner.setEnergieversorgungInProzent(2);
		romulaner.setPhotonentorpedoAnzahl(100);
		romulaner.setSchildeInProzent(100);
		romulaner.setAndroidenAnzahl(2);
		romulaner.setLebeneserhaltungssystemInProzent(0);
		romulaner.setSchiffname("IRW Kahazara");
		
		
		Ladungen romulanerLadung1 = new Ladungen();
		romulanerLadung1.setBezeichnung("Borg-Schrott");
		romulanerLadung1.setMenge(5);
		romulaner.addLadung(romulanerLadung1);
		
		Ladungen romulanerLadung2 = new Ladungen();
		romulanerLadung2.setBezeichnung("Rote Materie");
		romulanerLadung2.setMenge(2);
		romulaner.addLadung(romulanerLadung2);
		
		Ladungen romulanerLadung3 = new Ladungen();
		romulanerLadung3.setBezeichnung("Plasma-Waffe");
		romulanerLadung3.setMenge(50);
		romulaner.addLadung(romulanerLadung3);
		
		//Klingonen
		klingonen.setEnergieversorgungInProzent(100);
		klingonen.setPhotonentorpedoAnzahl(1);
		klingonen.setSchildeInProzent(100);
		klingonen.setAndroidenAnzahl(2);
		klingonen.setLebeneserhaltungssystemInProzent(0);
		klingonen.setSchiffname("IKS Hegh'ta");
		
		Ladungen klingonenLadung1 = new Ladungen();
		klingonenLadung1.setBezeichnung("Ferengi Schneckensaft");
		klingonenLadung1.setMenge(200);
		klingonen.addLadung(klingonenLadung1);
		
		Ladungen klingonenLadung2 = new Ladungen();
		klingonenLadung2.setBezeichnung("Bat'leth Klingonen Schwert");
		klingonenLadung2.setMenge(200);
		klingonen.addLadung(klingonenLadung2);
		
		//Vulkanier
		vulkanier.setEnergieversorgungInProzent(80);
        vulkanier.setPhotonentorpedoAnzahl(0);
        vulkanier.setSchildeInProzent(80);
        vulkanier.setAndroidenAnzahl(5);
        vulkanier.setHuelleInProzent(50);
        vulkanier.setSchiffname("NI'Var");
        vulkanier.setLebeneserhaltungssystemInProzent(100);
        
		Ladungen vulkanierLadung1 = new Ladungen();
		vulkanierLadung1.setBezeichnung("Forschungssonde");
		vulkanierLadung1.setMenge(35);
		vulkanier.addLadung(vulkanierLadung1);
		
		Ladungen vulkanierLadung2 = new Ladungen();
		vulkanierLadung2.setBezeichnung("Photonentorpedos");
		vulkanierLadung2.setMenge(3);
		vulkanier.addLadung(vulkanierLadung2);
		
		//Ausgabe der Artibute
		
		vulkanier.zustandausgeben();
		vulkanier.ladungsausgabe();
		
		
		romulaner.zustandausgeben();
		romulaner.ladungsausgabe();
		romulaner.abschießenPhaserkanone(klingonen);
		
		klingonen.zustandausgeben();
		klingonen.ladungsausgabe();
		klingonen.abschießenPhotonentorpedos(romulaner);
	}



	

}
